package com.nimit.democaptureapp.Activity.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.nimit.democaptureapp.Activity.Utils.Const;
import com.nimit.democaptureapp.Activity.square.CameraFragment;
import com.nimit.democaptureapp.R;

public class CameraCaptureActivity extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_CAMERA_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_capture);
        requestForCameraPermission();
    }


    public void requestForCameraPermission() {
        final String permission = Manifest.permission.CAMERA;
        final String storage_permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(CameraCaptureActivity.this, permission) == PackageManager.PERMISSION_GRANTED) {
            launch();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(CameraCaptureActivity.this, permission)) {
                showPermissionRationaleDialog("Need Camera Permission to Capture Photo", permission,storage_permission);
            } else {
                requestForPermission(permission,storage_permission);
            }
        }
    }

    public void setResult(String mCurrentPhotoPath)
    {
        Intent intent = new Intent();
        intent.putExtra(Const.EXTRA_PHOTO_PATH, mCurrentPhotoPath);
        setResult(RESULT_OK, intent);
        CameraCaptureActivity.this.finish();
    }
    private void showPermissionRationaleDialog(final String message, final String permission, final String storage_permission) {
        new AlertDialog.Builder(CameraCaptureActivity.this)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CameraCaptureActivity.this.requestForPermission(permission, storage_permission);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }

    private void requestForPermission(final String permission,final String storage_permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{permission,storage_permission}, REQUEST_CAMERA_PERMISSION);
        }
    }

    private void launch() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_container, CameraFragment.newInstance(), CameraFragment.TAG)
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launch();
                } else {
                    onBackPressed();
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
