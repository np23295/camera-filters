package com.nimit.democaptureapp.Activity.Adapter;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.nimit.democaptureapp.R;

public class FilterGridAdapter extends BaseAdapter {
    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.filtered, R.drawable.unfiltered,
            R.drawable.filtered, R.drawable.unfiltered,
            R.drawable.filtered, R.drawable.unfiltered,
            R.drawable.filtered, R.drawable.unfiltered,
            R.drawable.filtered, R.drawable.unfiltered,
            R.drawable.filtered, R.drawable.unfiltered,
    };
    private Context mContext;

    // Constructor
    public FilterGridAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(mContext.getResources().getDimensionPixelSize(R.dimen.filter_thumbnail_width), mContext.getResources().getDimensionPixelSize(R.dimen.filter_thumbnail_height)));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        }
        else
        {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(mThumbIds[position]);
        imageView.setAnimation(animation);
        return imageView;
    }
}