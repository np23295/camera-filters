package com.nimit.democaptureapp.Activity.Activity;


import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import com.nimit.democaptureapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String imgPath=getIntent().getStringExtra("path");
        AppCompatImageView imgView=(AppCompatImageView)findViewById(R.id.imgView);
        imgView.setImageURI(Uri.parse(imgPath));
    }
}
