package com.nimit.democaptureapp.Activity.Utils;

import android.util.Log;

import java.util.ArrayList;

public class Const {
    public static final String EXTRA_PHOTO_PATH = "file_path";
    public static class ScalpPhotoType {
        public final static String LEFT_SIDE = "leftSide";
        public final static String UPPER_FRONTAL = "upperFrontal";
        public final static String UPPER_REAR = "upperRear";
        public final static String RIGHT_SIDE = "rightSide";

        public static String getDisplayName(String scalpPhotoType) {

            if (scalpPhotoType.equals(Const.ScalpPhotoType.LEFT_SIDE))
                return "Left Side";
            else if (scalpPhotoType.equals(Const.ScalpPhotoType.RIGHT_SIDE))
                return "Right Side";
            else if (scalpPhotoType.equals(Const.ScalpPhotoType.UPPER_FRONTAL))
                return "Upper Frontal";
            else if (scalpPhotoType.equals(Const.ScalpPhotoType.UPPER_REAR))
                return "Upper Rear";
            return "";
        }

        public static ArrayList<String> spinnerItems() {
            ///TODO on Change, Change menu_gallery_dropdown file also.
            Log.d("photoListAdapter", "spinnerItems: Inside Method");
            ArrayList<String> spinnerArrayList = new ArrayList<>();
            spinnerArrayList.add("Left Side");
            spinnerArrayList.add("Right Side");
            spinnerArrayList.add("Upper Frontal");
            spinnerArrayList.add("Upper Rear");
            spinnerArrayList.add("All");
            spinnerArrayList.add(spinnerArrayList.size(), "View/Angle");
            return spinnerArrayList;
        }

    }
}
