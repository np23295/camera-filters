package com.nimit.democaptureapp.Activity.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nimit.democaptureapp.Activity.Utils.Const;
import com.nimit.democaptureapp.Activity.fragment.Camera2BasicFragment;
import com.nimit.democaptureapp.Activity.widget.CameraPreview;
import com.nimit.democaptureapp.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CameraCaptureActivity1 extends AppCompatActivity implements View.OnClickListener{

    private final int PERMISSION_ACCESS_CAMERA = 1;
    @BindView(R.id.button_capture)
    AppCompatButton btnCapture;
    @BindView(R.id.camera_capture_btnSwitch)
    AppCompatButton btnSwitch;
//    @BindView(R.id.imgTarget)
//    ImageView imgTarget;
    @BindView(R.id.camera_preview)
    FrameLayout preview;
    private Camera mCamera;
    private CameraPreview mPreview;
    private String TAG = "CameraCaptureActivity";
    private String mCurrentPhotoPath, photoType;
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            try {
                File pictureFile = createImageFile();
                if (pictureFile == null) {
                    Log.d(TAG, "Error creating media file, check storage permissions: ");
                    return;
                }

                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Intent intent = new Intent();
                intent.putExtra(Const.EXTRA_PHOTO_PATH, mCurrentPhotoPath);
                setResult(RESULT_OK, intent);
                CameraCaptureActivity1.this.finish();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_capture);
        ButterKnife.bind(this);

        btnCapture.setOnClickListener(this);
        btnSwitch.setOnClickListener(this);

        /*if (checkPermission(Manifest.permission.CAMERA, PERMISSION_ACCESS_CAMERA)) {
            init();
        }*/

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            btnCapture.setVisibility(View.GONE);
            btnSwitch.setVisibility(View.GONE);
            getFragmentManager().beginTransaction()
                    .replace(R.id.camera_preview, Camera2BasicFragment.newInstance())
                    .commit();
        }else{
            init(Camera.CameraInfo.CAMERA_FACING_BACK);
        }

        photoType = getIntent().getStringExtra(Const.EXTRA_PHOTO_PATH);
//        imgTarget.setImageResource(R.drawable.upperrear_transparent);
       /* if (photoType.equals(Const.ScalpPhotoType.UPPER_REAR)) {
            imgTarget.setImageResource(R.drawable.upperrear_transparent);
        } else if (photoType.equals(Const.ScalpPhotoType.RIGHT_SIDE)) {
            imgTarget.setImageResource(R.drawable.rightside_transparent);
        } else if (photoType.equals(Const.ScalpPhotoType.LEFT_SIDE)) {
            imgTarget.setImageResource(R.drawable.leftside_transparent);
        } else if (photoType.equals(Const.ScalpPhotoType.UPPER_FRONTAL)) {
            imgTarget.setImageResource(R.drawable.upperfrontal_transparent);
        }*/
    }

    private void init(int face) {
        if (checkCameraHardware()) {
            // Create an instance of Camera
            mCamera = getCameraInstance(face);
            mCamera.setDisplayOrientation(90);

            //STEP #1: Get rotation degrees
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break; //Natural orientation
                case Surface.ROTATION_90:
                    degrees = 90;
                    break; //Landscape left
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;//Upside down
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;//Landscape right
            }
            int rotate = (info.orientation - degrees + 360) % 360;

            //STEP #2: Set the 'rotation' parameter
            Camera.Parameters params = mCamera.getParameters();
            params.setRotation(rotate);
            mCamera.setParameters(params);

            // Create our Preview view and set it as the content of our activity.
            mPreview = new CameraPreview(this, mCamera);
            preview.addView(mPreview);
        }
    }

    private boolean checkCameraHardware() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    public Camera getCameraInstance(int face) {
        Camera c = null;
        try {
            c = Camera.open(face); // attempt to get a Camera instance
            Camera.Parameters param = c.getParameters();
            param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            c.setParameters(param);
        } catch (Exception e) {
            e.printStackTrace();
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_capture:
                mCamera.takePicture(null, null, mPicture);
                break;
            case R.id.camera_capture_btnSwitch:
                releaseCamera();
                if(btnSwitch.getText().toString().equals(getString(R.string.label_front))){
                    btnSwitch.setText(getString(R.string.label_back));
                    init(Camera.CameraInfo.CAMERA_FACING_BACK);
                }else{
                    btnSwitch.setText(getString(R.string.label_front));
                    init(Camera.CameraInfo.CAMERA_FACING_FRONT);
                }
                break;
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
            preview.removeAllViews();
        }
    }
}
